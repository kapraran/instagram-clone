class Story {
  String author;
  String authorImgUrl;

  Story({this.author, this.authorImgUrl});
}

final stories = [
  Story(author: 'Ria', authorImgUrl: 'assets/images/user0.png'),
  Story(author: 'Nick', authorImgUrl: 'assets/images/user1.png'),
  Story(author: 'Lesie', authorImgUrl: 'assets/images/user2.png'),
  Story(author: 'Maria', authorImgUrl: 'assets/images/user3.png'),
  Story(author: 'Katie', authorImgUrl: 'assets/images/user4.jpg'),
  Story(author: 'Ria', authorImgUrl: 'assets/images/user0.png'),
  Story(author: 'Nick', authorImgUrl: 'assets/images/user1.png'),
  Story(author: 'Lesie', authorImgUrl: 'assets/images/user2.png'),
  Story(author: 'Maria', authorImgUrl: 'assets/images/user3.png'),
  Story(author: 'Katie', authorImgUrl: 'assets/images/user4.jpg'),
];
