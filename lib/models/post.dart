class Post {
  String author;
  String authorImgUrl;
  String postImgUrl;
  int likes;
  int comments;

  Post(
      {this.author,
      this.authorImgUrl,
      this.postImgUrl,
      this.likes: 0,
      this.comments: 0});
}

final posts = [
  Post(
      author: 'Lisa Ann',
      authorImgUrl: 'assets/images/user0.png',
      postImgUrl: 'assets/images/post0.jpg'),
  Post(
      author: 'Katie',
      authorImgUrl: 'assets/images/user4.jpg',
      postImgUrl: 'assets/images/post1.jpg'),
];
