import 'package:flutter/material.dart';
import 'package:instagram_clone/models/post.dart';
import 'package:instagram_clone/models/story.dart';

class Feed extends StatefulWidget {
  @override
  FeedState createState() => FeedState();
}

class FeedState extends State<Feed> {
  Widget _buildPost(int index) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: BoxDecoration(
            color: Color(0xFFFFFFFF),
            borderRadius: BorderRadius.circular(16.0)),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: Container(
                width: 48.0,
                height: 48.0,
                decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                  BoxShadow(
                      color: Colors.black45,
                      offset: Offset(0, 2),
                      blurRadius: 4.0)
                ]),
                child: CircleAvatar(
                  child: ClipOval(
                    child: Image(
                      width: 48.0,
                      height: 48.0,
                      image: AssetImage(posts[index].authorImgUrl),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              title: Text(
                posts[index].author,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text('15 min'),
              trailing: IconButton(
                icon: Icon(Icons.more_horiz),
                onPressed: () => print('More...'),
              ),
            ),
            Container(
              height: 400.0,
              margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(16.0),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black45,
                      offset: Offset(0, 2),
                      blurRadius: 8.0,
                    )
                  ],
                  image: DecorationImage(
                      image: AssetImage(posts[index].postImgUrl),
                      fit: BoxFit.cover)),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.favorite_border),
                        iconSize: 28.0,
                        onPressed: () => print('Like post'),
                      ),
                      IconButton(
                        icon: Icon(Icons.comment),
                        iconSize: 28.0,
                        onPressed: () => print('Like post'),
                      )
                    ],
                  ),
                  IconButton(
                    icon: Icon(Icons.bookmark_border),
                    iconSize: 28.0,
                    onPressed: () => print('Save post'),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFEDF0F6),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text(
                    'Instagram',
                    style: TextStyle(fontSize: 32.0, fontFamily: 'Billabong'),
                  ),
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(Icons.live_tv),
                      iconSize: 24.0,
                      onPressed: () => print('IGTV'),
                    ),
                    IconButton(
                      icon: Icon(Icons.send),
                      iconSize: 24.0,
                      onPressed: () => print('Messages'),
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            height: 100.0,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: stories.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  margin: EdgeInsets.symmetric(horizontal: 8.0),
                  width: 56.0,
                  height: 56.0,
                  decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                    BoxShadow(
                        color: Colors.black45,
                        offset: Offset(0, 2),
                        blurRadius: 4.0)
                  ]),
                  child: CircleAvatar(
                    child: ClipOval(
                      child: Image(
                        width: 56.0,
                        height: 56.0,
                        image: AssetImage(stories[index].authorImgUrl),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          _buildPost(0),
          _buildPost(1)
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
              icon: Icon(
                Icons.dashboard,
                size: 24,
                color: Colors.black,
              ),
              title: Text('')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.search,
                size: 24,
                color: Colors.grey,
              ),
              title: Text('')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.add,
                size: 24,
                color: Colors.grey,
              ),
              title: Text('')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite_border,
                size: 24,
                color: Colors.grey,
              ),
              title: Text('')),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.person_outline,
                size: 24,
                color: Colors.grey,
              ),
              title: Text(''))
        ],
      ),
    );
  }
}
